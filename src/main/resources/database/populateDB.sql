-- developers
INSERT INTO developers VALUES (1, 'Ivan', 'Ivanon', 'Java Developer', 3000.00);
INSERT INTO developers VALUES (2, 'Kolya', 'Nikolaev', 'C++ Developer', 4000.00);

-- skills
INSERT INTO skills VALUES (1, 'Java');
INSERT INTO skills VALUES (2, 'SQL');
INSERT INTO skills VALUES (3, 'JDBC');